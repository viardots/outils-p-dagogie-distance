#!/bin/bash
TMPDATA=${TMPDATA:=/scratch}
# Crée un répertoire pour les installations locales (s'il n'est pas déjà créé)
mkdir -p ${TMPDATA}/$(id -un)/bin/
export PATH="/matieres/5MMSDSI/bin:${TMPDATA}/$(id -un)/bin/:${TMPDATA}/$(id -un)/usr/bin/":${PATH}
export VBOX_USER_HOME="${TMPDATA}/$(id -un)/.config/VirtualBox"
export XDG_CONFIG_HOME="${VBOX_USER_HOME}"
#if which socat
#then
#  echo "socat est installé"
#else
#  (
#    cd ${TMPDATA}/$(id -un)
#    apt download socat
#    dpkg -x socat*.deb "${TMPDATA}/$(id -un)/"
#  )
#fi 
#if [ "$DISPLAY" != "" ]
#then
#  DISPLAY_NUMBER=$(echo $DISPLAY | cut -d. -f1 | cut -d: -f2)
#  PORTSOCKETX11=$(lsof -i -P -n | grep LISTEN | wc -l)
#  if [ "$PORTSOCKETX11" != "1" ]
#  then
#    socat TCP-L:60${DISPLAY_NUMBER},fork UNIX:/tmp/.X11-unix/X${DISPLAY_NUMBER} &
#  fi
#fi
# Démarrage de docker
## Mise en place de l'environnement local pour virtualbox sur /scratch
MACHINEFOLDER=$(vboxmanage list systemproperties | grep "Default machine folder:" | awk '{print $4}')
mkdir -p ${TMPDATA}/$(id -un)/VirtualboxVMs/
chmod 700 ${TMPDATA}/$(id -un)
## Vérifie que l'environnement n'est pas déjà défini comme il faut en utilisant ${TMPDATA}
if [[ "$MACHINEFOLDER" == "${TMPDATA}/$(id -un)/VirtualboxVms/" ]]
then
  vboxmanage setproperty machinefolder ${TMPDATA}/$(id -un)/VirtualboxVms/
fi
## Vérification de l'existance de VM docker 
DOCKERVM=$(vboxmanage list vms | egrep '^"default"')
RUNNINGDOCKERVM=$(vboxmanage list runningvms | egrep '^"default"')
if [ ! "$DOCKERVM" == "" ] && [ "$RUNNINGDOCKERVM" == "" ]
then
  docker-machine -s "${TMPDATA}/$(id -un)/.docker/machine" rm default -f
fi
if [ "$DOCKERVM" == "" ] || [ "$RUNNINGDOCKERVM" == "" ]
then
  ## S'il n'y a pas de VM docker on la crée
  rm -rf ${TMPDATA}/$(id -un)/.docker
  ## Suppression des machines virtuelles s'il y en a
  ## vboxmanage list vms | awk '{id=match($2,/\{(.*)\}/,m);print (m[1])}' | xargs -I {} vboxmanage controlvm {} poweroff
  ## vboxmanage list vms | awk '{id=match($2,/\{(.*)\}/,m);print (m[1])}' | xargs -I {} vboxmanage unregistervm {} --delete
  ## Création de la docker-machine pour travailler dans un environnement isolé avec la dernière version de docker
  vboxmanage list hostonlyifs | grep -e "^Name" | awk '{print$2}' | xargs -I {} vboxmanage hostonlyif remove {}  
  docker-machine -s "${TMPDATA}/$(id -un)/.docker/machine" create --driver virtualbox --virtualbox-share-folder "/:hostmachine" --virtualbox-cpu-count "2" --virtualbox-memory "2048" --virtualbox-hostonly-cidr 192.168.56.1/21 default
  #docker-machine -s "${TMPDATA}/$(id -un)/.docker/machine" ssh default "wget https://download.docker.com/linux/static/stable/x86_64/docker-24.0.6.tgz;tar xvzf docker-24.0.6.tgz;sudo mv docker/* /usr/local/bin;sudo /etc/init.d/docker restart"
  ssh -i "${TMPDATA}/$(id -un)/.docker/machine/machines/default/id_rsa" -n -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" docker@$(docker-machine -s "${TMPDATA}/$(id -un)/.docker/machine" ip default)  "curl -L https://download.docker.com/linux/static/stable/x86_64/docker-27.2.1.tgz | tar xvz;sudo mv docker/* /usr/local/bin;sudo /etc/init.d/docker restart"
fi
## Mise en place des variables d'environnement si nécessaire
eval $(docker-machine -s "${TMPDATA}/$(id -un)/.docker/machine" env default)
export DOCKER_MACHINE_IP=$(docker-machine -s "${TMPDATA}/$(id -un)/.docker/machine" ip)
cat <<EOF
Vous pouvez ajouter dans votre .bashrc les lignes suivantes pour que les autres terminaux soient correctement configurés:
eval $(docker-machine -s "${TMPDATA}/$(id -un)/.docker/machine" env default)
export DOCKER_MACHINE_IP=$(docker-machine -s "${TMPDATA}/$(id -un)/.docker/machine" ip)
export PATH="/${TMPDATA}/$(id -un)/bin/":${PATH}
EOF
