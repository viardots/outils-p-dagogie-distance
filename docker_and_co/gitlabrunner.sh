#!/bin/bash
# Nécessite d'avoir une variable TOKEN avec le token du groupe pour lancer le runner.
TMPDATA=${TMPDATA:=/scratch}
GITLABURL=${GITLABURL:=https://gitlab.ensimag.fr}
# Vérifie l'installation de gitlab-runner
mkdir -p ${TMPDATA}/$(id -un)/bin/
mkdir -p ${TMPDATA}/$(id -un)/.gitlab-runner/
CONFIGGITLABRUNNER="${TMPDATA}/$(id -un)/.gitlab-runner/config.toml"
export PATH="${TMPDATA}/$(id -un)/bin/":${PATH}
if which gitlab-runner
then
  echo "Gitlab-runner installé"
else
  if [ ! -f "${TMPDATA}/$(id -un)/bin/gitlab-runner" ]
  then
    curl -L --output "${TMPDATA}/$(id -un)/bin/gitlab-runner" "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
    chmod +x  "${TMPDATA}/$(id -un)/bin/gitlab-runner"
  fi
fi
gitlab-runner register -n -c "${CONFIGGITLABRUNNER}" --url "${GITLABURL}" --registration-token "${TOKEN}"  \
              --docker-privileged --executor docker --description "docker local ${HOSTNAME}" \
              --tag-list=ecole,docker,${HOSTNAME},${USER} \
              --docker-pull-policy="if-not-present" \
              --docker-image "docker:stable" \
              --run-untagged="true" --locked="false"
gitlab-runner run -c "${CONFIGGITLABRUNNER}"
gitlab-runner unregister -c "${CONFIGGITLABRUNNER}" --all-runners
