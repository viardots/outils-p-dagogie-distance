#!/bin/bash
TMPDATA=${TMPDATA:=/scratch}
LABTAINER=${TMPDATA}/$(id -un)/labtainer
if [ ! -d ${LABTAINER} ]
then
  mkdir -p ${LABTAINER}
  cd ${LABTAINER}/..
  curl -L https://github.com/mfthomps/Labtainers/releases/download/v1.3.7r/labtainer.tar \
  --output labtainer.tar
  tar xf labtainer.tar
  curl -L https://viardots.gricad-pages.univ-grenoble-alpes.fr/outils-p-dagogie-distance/labutils.py --output ${LABTAINER}/trunk/scripts/labtainer-student/bin/labutils.py
  curl -L https://viardots.gricad-pages.univ-grenoble-alpes.fr/outils-p-dagogie-distance/labtainer --output ${LABTAINER}/trunk/scripts/labtainer-student/bin/labtainer
  chmod +x ${LABTAINER}/trunk/scripts/labtainer-student/bin/labtainer
fi
export LABTAINER_DIR=${LABTAINER}/trunk
export PATH=${LABTAINER_DIR}/scripts/labtainer-student/bin:$PATH
cd "${LABTAINER_DIR}/scripts/labtainer-student"
echo "Dans cette console taper: labtainer"
