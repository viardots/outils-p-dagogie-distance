# Outils numériques pour la pédagogie

## Documentations de l'école

### Service info

- <http://intranet.ensimag.grenoble-inp.fr/fr/informatique/liste-des-pc-ensimag-accessibles-a-distance-1?ksession=6ddfb2d5-3203-48a0-a84d-c16b6efd2015>
- Machines dispos : <https://intranet.ensimag.fr/corona>

### Bug busters

- <https://wiki.bubu-ensimag.fr/start>
- <https://wiki.bubu-ensimag.fr/wiki_ensimag/matieres/1a_cep>

## Interaction avec les étudiants

### Les outils pour la prise en main à distance

#### Boitier de vote

Pour impliquer les participants d'une présentation/cours, le système de boitier de vote
est une solution intéressante.

##### Documentation rapide

###### Mise en place

0. Avoir préparé son quiz en utilisant le modèle <https://bvote.ensimag.fr/questions_supportees.yml>
1. Coté animateur/prof : se rendre à <https://bvote.ensimag.fr/#/teacher/login>
2. S'identifier, l'animateur/prof est redirigé vers <https://bvote.ensimag.fr/#/teacher/>**login_animateur**
3. Cliquer sur la gauche sur la flêche pour charger le quiz
4. Et voilà... c'est prêt les participants peuvent se connecter à <https://bvote.ensimag.fr/#/student/>**login_animateur** avec leur téléphone, ordinateur, tablette. Un simple accès à internet suffit. Un QRCODE peut être affiché pour un accès à la page de l'animateur pour faciliter l'accès, ainsi que l'adresse à taper par les participants (cliquer sur l'icône en forme de QRCode en haut à gauche pour l'afficher, et recliquer au même endroit pour revenir aux questions).

###### Animation de la session

Le dispositif mis en place,

1. l'animateur/prof sélectionne la question, les participants/élèves ont un accès à la question et peuvent répondre. L'idée est de projeter la vue prof aux élèves pour faire l'animation.
2. Quand un participant répond, sa réponse est enregistrée et est mise en évidence. Il doit attendre une action de l'animateur/prof pour accéder à une autre question.
3. L'animateur/prof voit en direct le nombre de personnes ayant répondu à la question, il peut :

- montrer en temps réel (en cliquant sur l'oeil) les réponses des participants. **Conseil** : ça peut être intéressant de le faire au bout d'un certain temps (donc pas tout de suite) pour inciter à chercher dans un premier temps, et pour désinhiber ceux qui n'osent pas ensuite.
- montrer les bonnes réponses (en cliquant sur l'oeil - valable pour les questions à choix multiple). **Conseil** : Il peut être pertinent d'expliquer les raisonnements pour répondre à la question, afin que les participants se projettent sur ce qu'ils auraient du répondre après cette explication, la révélation des réponses jugées justes peut être encore mieux reçue. **Remarque** : Parfois il n'y a pas de bonne ou mauvaise réponse, c'est aussi l'occasion de susciter une réaction et de réfléchir à la question, c'est encore plus formateur dans ce cas.
- afficher un nuage de mots (en cliquant sur le nuage - valable pour les réponses libres). **Conseil** : Démarrer une animation en posant une question ouverte sur le sujet qu'on va traiter permet d'impliquer rapidement les participants dans la session. On peut facilement sonder, visualiser les mots qu'ils associent au sujet et adapter le discours. **Conseil** : Si on attend une réponse plus précise, essayer de poser une question sans trop d'ambiguïté.
- Réinitialiser le quiz (en cliquant sur le flêche qui reboucle), cela permet aux étudiants de re-répondre.
- Changer de question (en avant ou en arrière)

###### Questions supportées

Voir l'exemple <https://bvote.ensimag.fr/questions_supportees.yml>

3 types de questions existent :

- Questions ouvertes (type : **O**)
- Questions à choix unique (type : **C**), peuvent être aussi utilisées pour poser des questions de type Oui/Non/Je ne sais pas
- Questions à choix multiples (type : **M**)

La syntaxe markdown peut être utilisée, et notamment pour mettre des extraits de code.
Les équations mathématiques peuvent être décrites dans les balises **math**
Des images peuvent être insérées (syntaxe markdown), dans ce cas elles doivent être mises sur un site d'accès public.

#### Partage de session en mode graphique (utilise vnc et novnc) (assistance poste école)

Les infos sur <https://intranet.ensimag.grenoble-inp.fr/fr/informatique>

##### Quick start

- Se connecter au VPN de Grenoble INP
- Se connecter sur une machine libre de l'école en ssh (cf. <https://intranet.ensimag.fr/corona>, http://clonage.ensimag.fr/board/ (VPN))
  - depuis 2021, il faut allumer à distance les PC par mail s'ils ne le sont pas déjà

```bash
ssh ensipc210.ensimag.fr
```

- Lancer la commande :

```bash
vnc_web.sh start
````

- Ouvrir un navigateur et se rendre à l'url indiquée (ex: <https://ensipc210.ensimag.fr:6080>), accepter le certificat tls qui permet un échange chiffré, et saisir le mot de passe donné après vnc_web.sh start

- Profiter

- Arrêter avec :

```bash
vnc_web.sh stop
```

<img alt="capture vnc_web" src="./vnc_web.gif" width="40%">

#### Info complémentaires

Au premier lancement il crée :

- un certificat d'un autorité de certification qui vous appartient (**.vnc/monca.crt**), il peut être intéressant de l'ajouter dans les certificats de confiance de son navigateur ou son système
- un certificat signé par l'autorité de certification pour la machine sur laquelle on lance la commande vnc_web.sh start. Ce certificat sera utilisé pour les échanges https.
- un fichier de configuration .vnc/xstartup qui lancera Gnome comme gestionnaire de fenêtre (peut être modifié si vous voulez changer de gestionnaire de fenêtre)

A chaque lancement il crée :

- un mot de passe pour la prise en main à distance (peut être partagé pour travailler à plusieurs)
- un mot de passe pour partager l'écran sans prise en main
- lance un serveur VNC accessible uniquement en localhost avec le mot de passe donné ci-dessus
- lance une interface web accessible uniquement en https sur le port 6080 permettant un accès à la machine.

##### Listes des options

```bash
/usr/local/bin/vnc_web.sh start|stop|passwd|share|unshare|cert
   start : démarre le service qui sera accessible à https://ensipc210.ensimag.fr:6080
   stop : arrete le service, à faire dès que vous n'en avez plus besoin pour laisser la machine disponible
   passwd : renouvelle les mots de passe d'accès à novnc, fait automatiquement à chaque start
   share : permet de changer temporairement de mot de passe pour les donner à une personne de confiance le temps de travailler ensemble
   unshare : rétablit les mots de passe de base (soit obtenu lors du start soit après passwd) et déconnecte tous ceux déjà connectés (vous y compris)
   cert : regénère le certificat pour la machine courante (nécessite de recharger la page et d'accepter l'exception de sécurité au niveau du navigateur)
   cacert : regénère le certificat racine (à ajouter aux certificats CA du navigateur)
````

#### Partage de session en mode console : tmate <https://tmate.io/>

```bash
tmate 
# puis demander à afficher les urls à utiliser pour la prise en main à distance

tmate show-messages 
Mon Mar 16 16:42:56 2020 [tmate] Connecting to ssh.tmate.io...
Mon Mar 16 16:42:57 2020 [tmate] Note: clear your terminal before sharing readonly access
Mon Mar 16 16:42:57 2020 [tmate] web session read only: https://tmate.io/t/ro-...
Mon Mar 16 16:42:57 2020 [tmate] ssh session read only: ssh ro-...@lon1.tmate.io
Mon Mar 16 16:42:57 2020 [tmate] web session: https://tmate.io/t/...
Mon Mar 16 16:42:57 2020 [tmate] ssh session: ssh ...@lon1.tmate.io

```

Ensuite il suffit de fournir l'url donnée (soir l'url ro pour une démo, soit l'autre pour prendre la main sur une session étudiant)

Pour quitter sans fermer tout : ctrl-b d (et non crtl-b ctrl-d ni ctrl-d)

#### Assistance d'un étudiant à distance en mode graphique (poste perso)

Utilisation de Dayon (<https://retgal.github.io/Dayon/fr_index.html>)

##### Poste assistant (le prof)

Le faire depuis un poste avec une adresse accessible depuis internet (par exemple une machine de l'école avec vnc_web.sh si l'étudiant est sur le vpn):

- Télécharger l'archive, et la décompresser

```bash
wget https://github.com/RetGal/Dayon/releases/download/v1.10.2/dayon.1.10.2.tgz
tar xvzf dayon.1.10.2.tgz
cd dayon/bin
./dayon_assistant.sh
```

Peut se lancer la première fois en une ligne

```bash
wget https://github.com/RetGal/Dayon/releases/download/v1.10.2/dayon.1.10.2.tgz;tar xvzf dayon.1.10.2.tgz;cd dayon/bin;java -cp dayon.jar  mpo.dayon.assistant.AssistantRunner
```

- Changer éventuellement le port utiliser avec l'icône en forme de réseau
- démarrer avec l'icône en forme de lecture.
- Communiquer l'adresse de la machine et le port associé à l'étudiant qui a besoin d'assistance.
- Pour prendre la main cliquer sur la main

##### Poste assisté (élève)

1. Installation

Le plus simple est de suivre les indications sur le site de l'outil en fonction de votre système d'exploitation:
<https://retgal.github.io/Dayon/fr_index.html>

Sinon sous OSX et linux, vous pouvez le faire en une ligne dans une ligne de commande :

```bash
wget https://github.com/RetGal/Dayon/releases/download/v1.10.2/dayon.1.10.2.tgz;tar xvzf dayon.1.10.2.tgz;cd dayon/bin;java -cp dayon.jar  mpo.dayon.assisted.AssistedRunner
```

### La messagerie instantanée riot

- riot <https://riot.ensimag.fr>

1. On peut se connecter directement avec les identifiants de l'établissement.
2. On peut créer des salons et inviter les élèves
3. Pour discuter directement avec un étudiant:
        - soit le sélectionner dans une conversation et cliquer sur l'étudiante et dans le menu à droite DISCUSSIONS DIRECTES -> Commencer une conversation
        - soit à gauche dans PERSONNES, cliquer sur le + et "Commencer une conversation"
4. On peut modifier un message qu'on a écrit, copier-coller des images, insérer un fichier, ...

### Discord comme outil d'animation

<https://discordapp.com/>

Discord, commme riot permet de créer des salons, avoir des échanges vocaux et de partager facilement son écran (plus difficile avec riot).

Il faut se créer un "serveur" qui est en fait un groupement de salons avec un administrateur qui peut donner des droits.

Basé sur un principe de salons textes et de salons vocaux, on sélectionne un salon texte pour le chat et un salon vocal pour les échanges voix/partage écran/visio.
L'interface montre bien dans quel salon vocal est tel ou tel participant (et on ne peut être que dans un salon à la fois). C'est comme si on se déplaçait virtuellement d'une salle à une autre.

En projet avec des groupes différents c'est une bonne alternative à riot, pour assurer aussi un live c'est une bonne alternative à youtube, par contre attention c'est limité à 50 participants!

Exemple d'utilisation en projet avec des groupes différents:

- Un salon général de texte pour les questions aux profs, et des salons textes par groupe.
- Un salon général vocal, et des salons vocaux par groupe.
- Les étudiants restent dans leur salon pour travailler, échanger (du coup tout le monde n'entend que ce qui le concerne)
- Les profs peuvent passer de salon en salon pour voir comment ça se passe, et échanger (via les salons vocaux) avec les étudiants et même faire des démos.
- Si les élèves veulent poser une question à un prof, ils peuvent identifier dans quel salon vocal il est , et aller le voir, ou poser une question dans le salon textuel général.

Essayé en projet à GI, plutôt bien adapté.

### Zoom pour la visio (doc venant de la DSI)

<https://zoom.us/>

- Toute personne qui souhaite initier une visioconférence s'enregistre sur <https://zoom.us/> avec son adresse mail professionnelle (@grenoble-inp.fr) mais avec un mot de passe spécifique à Zoom, complètement différent du mot de passe habituel
- Cette personne planifie ses visioconférences de manière autonome et communique l'URL pour se joindre ou utiliser la fonction Copier l'invitation et l'envoie par mail à ses correspondants

#### Breakout rooms (travail en groupe de 5 étudiants)

Les [breakout rooms](https://support.zoom.us/hc/en-us/articles/206476313) permet de plancher par exemple par groupe de 5 étudiants sur un même exercice. Ainsi, si vous avez 25 étudiants, vos étudiants seront automatiquement (ou manuellement) ventilés par groupe de 5 étudiants dans 5 conférences Zoom. En tant qu'animateur, vous pourrez vous ballader entre ces différents visios afin de voir l'avancement de chaque groupe. Eux-mêmes auront la possibilité de vous notifier s'ils ont besoin d'aide. Un déroulé classique pourrait être :

- annoncer la durée de travail sur l'exercice
- lancer les breakout rooms
  - n'oubliez pas d'activer préalablement le partage d'écran pour tous
  - l'assignation peut être automatique (les étudiants sont automatiquement ventilés dans les salles) ou manuelle (les étudiants rejoignent par eux-mêmes les salles)
    - il vaut mieux privilégier une réparition automatique car certains clients zoom ne voient pas les salons créés et ne peuvent les rejoindre
    - on peut alors les répartir à la main mais c est fastidieux et aléatoire
- laisser les étudiants travailler sur l'exercice. Généralement :
  - un étudiant partage son écran avec l'énoncé
  - il annote le pdf au fur et à mesure des échanges (MacOS : preview, Linux : okular ou [autres](https://itsfoss.com/pdf-editors-linux/), Windows : Acrobat Reader)
  - ou utiliser un [tableau blanc collaboratif](README.md#tableau-blanc)
- si l'enseignant n'est pas sollicité, il peut se ballader entre les salons pour voir l'avancement de chaque groupe
- une fois le temps écoulé, l'enseignant peut
  - envoyer un message à tous les étudiants les avertissant de la fin du temps règlementaire
  - fermer tous les salons (60s configurables)
  - ce qui avertit automatiquement tous les étudiants et les rapatrie dans le salon initial
  - pour une correction ou conclusion collective

Si vous faites appel plusieurs fois aux breakout rooms lors d'une même séance zoom, la répartition précédente des étudiants est mémorisée et est par défaut appliquée.

### Sauvegarder l'interaction avec un terminal

#### script et scriptreplay Disponible sur tous les postes de l'école

Cette procédure permet d'enregistrer une démo dans une console, puis de la rejouer quand on veut.
Peut faciliter une démo, ou peut même être donné à un étudiant pour montrer

```bash
# Pour enregistrer
script --timing=time.txt script.log 
# Faire ce qu'on a à faire, et finir par CTRL+d
...
[CTRL+d]
# Maintenant on peut rejouer le film 
scriptreplay --timing=time.txt script.log

```

Soit on l'utilise avec tmate, soit on peut fournir les 2 fichiers et expliquer comment les rejouer

#### <https://asciinema.org/> autre alternative

## La production de Video

### OBS Studio <https://obsproject.com/fr>

- Permet de se passer de montage.
- Sur mac j'ai du installer à partir des sources, la version actuelle (24.0.6) faisant clignoter l'écran.
- Explication en vidéo pour l'installation : <https://videos.univ-grenoble-alpes.fr/video/6994-installer-et-configurer-le-logiciel-obs/>
- Explication en vidéo pour l'utilisation des sources et scènes : <https://videos.univ-grenoble-alpes.fr/video/6995-le-concept-de-source-et-de-scene-avec-obs/>
- Explication en vidéo pour personnaliser et enregistrer : <https://videos.univ-grenoble-alpes.fr/video/6996-personnaliser-et-enregistrer-une-video-avec-obs/>

### Montage, outil suggéré par l'UGA Open Shot

- le site <https://www.openshot.org/fr/>
- la vidéo qui explique tout <https://videos.univ-grenoble-alpes.fr/video/6998-faire-du-montage-video-avec-le-logiciel-open-shot/>

### Montage en ligne de commande, surtout pour couper et redimensionner

- ffmpeg <https://www.ffmpeg.org/>
- Un aide mémoire <https://www.linuxtricks.fr/wiki/ffmpeg-la-boite-a-outils-multimedia>

#### Quelques exemples

- Pour couper un fichier entre les instants 10s01 et 56s

```bash
ffmpeg -ss 00:00:10.01 -to 00:00:56 -i Original.mp4 Copie_tronquée.mp4
```

voir <https://www.linuxtricks.fr/wiki/ffmpeg-la-boite-a-outils-multimedia#paragraph-couper-une-video>

- Pour extraire une image d'une vidéo:

```bash
ffmpeg -ss 00:00:00 -t 00:00:00  -i Original.mp4 -frames:v 1 Image_extraite.png
```

C'est utile:

1. pour pouvoir identifier les coordonnées précises (avec Gimp par exemple) d'une partie de l'image en vu de la rogner par exemple (cf. suite).
2. pour disposer d'une vignette pour la vidéo
3. pour oublier la vidéo et extraire seulement les captures écran de la vidéo pour en faire un document, une page web, ...

- Pour rogner l'image de la vidéo à partir des coordonnées (x=23, y=12) pour une taille de largeur=640 et hauteur=480 (donc de coordonnées x=640+23, y=480+12)

```bash
ffmpeg -i Original.mp4 -filter:v "crop=663:492:23:12" Copie_rognée.mp4
```

voir <https://www.linuxtricks.fr/wiki/ffmpeg-la-boite-a-outils-multimedia#paragraph-rogner-une-video>

- Pour ajouter des chapitres à la vidéo cf. <https://ffmpeg.org/ffmpeg-formats.html#Metadata-1>
  - le service de [chapitrage de vidéos de l'UGA](https://videos.univ-grenoble-alpes.fr/) est toutefois plus pratique

### Mise en ligne

- Via gitlab et pages (à compléter)
- Via <https://videos.univ-grenoble-alpes.fr/> -> <https://videos.univ-grenoble-alpes.fr/video/7001-depot-de-videos-sur-le-serveur-uga/>
- Via <https://videos.univ-grenoble-alpes.fr/> -> Utiliser le script du dépôt [/pod_tools/README.md](/pod_tools/README.md)

- Via youtube, surtout si on veut faire un direct live !!!

### Live avec YouTube

- Se créer un compte sur google
- Se créer sa chaine...
- Dans l'icone de profil (en haut à droite) séléctionner -> Votre chaîne
- Puis Youtube Studio en haut à droite
- Demander à pouvoir faire des directs (en cliquant sur le bouton qui ressemble à ça ((o)) ) ATTENTION IL FAUT 24h AVANT DE POUVOIR FAIRE VOTRE PREMIER DIRECT

- Puis Vidéos à gauche  pour retrouver vos vidéos (soit celles mises en ligne, soit les vidéos en direct qu'on peut récupérer)
- Sur OBS :
        - Paramètres -> Stream(flux) -> Service : Youtube/ Youtube Gaming
        - Cliquer sur le bouton Get Stream Key (qui ouvre un nouveau navigateur sur Youtube et crée un nouveau direct)
        - Copier la clé du flux.
        - Et voilà vous pouvez démarrer votre direct...
- pour le lien à partager -> sur Youtube Studio -> Videos -> Diffusions en direct, il y a une image sous Diffusions en direct à venir, en passant la souris dessus vous pouvez dans les options créer un lien de partage.
- Lien à diffuser

## Outils collaboratifs

### tableau blanc

L'outil idéal serait :

- sans inscription
- multi-utilisateur
- permettant d'importer des pdf, des images, ...
- fonctionnant également sur un smartphone
- ...

(pour modifier le tableau, utilisez <https://tableconvert.com/> pour une mise en forme facile)

| Outils                                           | sans inscription   | import                       | export  | annotation             | multiuser          | smartphone         |
|--------------------------------------------------|--------------------|------------------------------|---------|------------------------|--------------------|--------------------|
| <https://www.notebookcast.com/>                  | :white_check_mark: | image, ~~pdf~~               | image   | :white_check_mark:     | :white_check_mark: | :white_check_mark: |
| [excalidraw](https://excalidraw.com)             | :white_check_mark: | ~~image, pdf~~(03/2021)      |         |                        | :white_check_mark: |                    |
| <https://miro.com/>                              | :x:                | image, pdf                   |         |                        | :white_check_mark: |                    |
| [google draw](https://docs.google.com/drawings/) | :x:                | image, pdf                   |         | :white_check_mark:     | :white_check_mark: |                    |
| zoom annotation                                  | :x: (INP)          | partage écran                | capture | instable               | :white_check_mark: |                    |
| <https://aggie.io>                               | :white_check_mark: | image, ~~pdf~~               |         | ~~texte~~, ~~ligne~~,  | :white_check_mark: |                    |
| ...                                              |                    |                              |         |                        |                    |                    |

Veille outils :

- <https://alternativeto.net/feature/collaborative-whiteboard/>

### Live share de Visual Studio Code (doc de Grégory Natter)

#### Prérequis

1. Chaque élève doit avoir un compte Microsoft ou Github.

#### Installation

1. Ouvrez VSCode

2. Dans le menu de gauche, cliquez sur l'onglet "Extensions" (des petits carrés).

3. Cherchez "Live Share" (édité par Microsoft)

4. Cliquez sur le carré vert "Install"

5. Attendez puis cliquez sur le carré bleu "Reload"

6. Attendez qu'un onglet "Live Share" apparaisse dans le menu de gauche (peut prendre quelques minutes)

#### Démarrer une session en tant qu'hôte

1. Ouvrez VSCode

2. Cliquez sur l'onglet "Live Share"

3. Double cliquez sur "Start collaboration session..." et patienter

4. Si cela n'a pas déjà été fait, VSCode vous demande de vous authentifier avec un compte Microsoft ou Github

5. Dans la partie "Contact" de "Live Share", cliquez sur "Invite participant ..."

6. Si cela n'a pas déjà été fait, VSCode vous demande de vous authentifier avec un compte Microsoft ou Github

7. Une info bulle en bas à droite de l'écran vous permet de copier le lien d'invitation à la session (cette info bulle permet également de mettre le Live Share en read only).

8. Envoyez ce lien aux autres élèves de la session.

9. Attention : les autres participants ne pourront voir et éditer seulement les fichiers ouverts après leur connexion (n'hésitez pas, en cas de bug, à fermer et ouvrir les fichiers).

#### Rejoindre une session

1. Demandez le lien à l'hôte

2. Ouvrir VSCode

3. Cliquez sur l'onglet "Live Share"

4. Double cliquez sur "Join collaboration session ..."

5. Si cela n'a pas déjà été fait, VSCode vous demande de vous authentifier avec un compte Microsoft ou Github

6. Collez le lien dans la zone prévue à cet effet en haut de l'écran.

7. Si cela n'a pas déjà été fait, VSCode vous demande de vous authentifier avec un compte Microsoft ou Github

8. Appuyez sur entrer.

9. Vous pouvez maintenant suivre en direct les modifications (et interagir). Pour arrêter de suivre les mouvements de l'hôte, vous pouvez cliquer sur le pin vert en haut à droite de l'écran (Unfollow participant).

10. A la fin de la session, les documents restent stockés, par défaut, uniquement sur l'ordinateur hôte.

## Ipad

Si vous disposez d'un Ipad avec un pencil :

- vous avez la possibilité de [partager directement l'écran de votre ipad](https://support.zoom.us/hc/en-us/articles/201379235-Sharing-your-screen-iOS-with-the-Zoom-desktop-client) dans une visio zoom
- d'importer et annoter vos slides pdf dans [notability](https://www.gingerlabs.com/) (iOS):
  - la barre d'annotation est particulièrement riche
  - une barre d'annotation réduite avec uniquement vos [outils favoris](https://support.gingerlabs.com/hc/en-us/articles/360048463032-Favorite-Tools)
  - [dessiner des formes automatiquement](https://support.gingerlabs.com/hc/en-us/articles/226905028-Shapes-iOS-) (ligne droite, rectangle, cercle, triangle, ...)
  - :thumbsup: vous pouvez masquer les corrections d'un exercice en dessinant par dessus en blanc et les dévoiler, au fur et à mesure de la correction, en les [gommant partiellement ou totalement](https://support.gingerlabs.com/hc/en-us/articles/360029432891-Partial-Eraser).
  - ...
- ou d'utiliser le système d'[annotation de keynote](https://bcourses.berkeley.edu/courses/1462694/pages/how-to-annotate-your-presentation-slides?module_item_id=15494654) sur ipad
  - contrairement au pdf, vous bénéficiez des animations de keynote
  - maintener votre pencil sur un slide --> la barre d'annotation (rudimentaire) fait son apparition
    - en 4/3, la barre empiète sur vos slides. Elle disparaitra automatiquement si vous annotez ou pointer le laser par dessus
    - en 16/9, elle n'empiètera pas sur vos transparents
  - vous pouvez y choisir le pointeur laser
  - si vous souhaitez effacer tous les annotations, maintener le pencil sur l'icone "undo" --> un menu fera son apparition avec l'action "tout effacer"
  - :warning: les annotations sont temporaires et disparaitront si vous arrêtez la présentation en plein écran
