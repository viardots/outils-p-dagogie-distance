#!/bin/bash -x
if [  -d /scratch ]; then
    TMPDATA=/scratch
else
    TMPDATA=/tmp
fi
USERDIR=${TMPDATA}/$(id -un)
DOCKERMACHINEDIR=$USERDIR/.docker/machine
DOCKERMACHINESDIR=$DOCKERMACHINEDIR/machines
CACHEDOCKER=$DOCKERMACHINEDIR/cache
MACHINENAME=default
MACHINEDIR=$DOCKERMACHINESDIR/$MACHINENAME
PWDMACHINE=tcuser
# Arrête toutes les VMS présentes
vboxmanage list vms | gawk '{id=match($2,/\{(.*)\}/,m);print (m[1])}' | xargs -I {} vboxmanage controlvm {} poweroff
vboxmanage list vms | gawk '{id=match($2,/\{(.*)\}/,m);print (m[1])}' | xargs -I {} vboxmanage unregistervm {} --delete
rm -rf $MACHINEDIR $CACHEDOCKER
