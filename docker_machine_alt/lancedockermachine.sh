#!/bin/bash -x
if [  -d /scratch ]; then
    TMPDATA=/scratch
else
    TMPDATA=/tmp
fi
USERDIR=${TMPDATA}/$(id -un)
echo "== Installation dans le répertoire ${USERDIR} =="
DOCKERMACHINEDIR=$USERDIR/.docker/machine
DOCKERMACHINESDIR=$DOCKERMACHINEDIR/machines
CACHEDOCKER=$DOCKERMACHINEDIR/cache
MACHINENAME=default
MACHINEDIR=$DOCKERMACHINESDIR/$MACHINENAME
PWDMACHINE=tcuser
# Configure le compte utilisateur pour travailler sur une espace local à la machine
mkdir -p ${USERDIR}/VirtualboxVms/ && vboxmanage setproperty machinefolder ${USERDIR}/VirtualboxVms/
chmod 700 ${USERDIR}
# Arrête toutes les VMS présentes
echo "== Arrête toutes les machines virtuelles en cours =="
vboxmanage list vms | gawk '{id=match($2,/\{(.*)\}/,m);print (m[1])}' | xargs -I {} vboxmanage controlvm {} poweroff &> /dev/null

# Récupère l'ISO permettant d'initialiser la VM hébergeant la machine docker
echo "== Télécharge l'image ISO boot2docker.iso =="
(
  mkdir -p $CACHEDOCKER
  mkdir -p $MACHINEDIR
  cd $CACHEDOCKER
  curl -L https://github.com/boot2docker/boot2docker/releases/download/v19.03.12/boot2docker.iso -O
  cp $CACHEDOCKER/boot2docker.iso $MACHINEDIR
)

if command -v sshpass > /dev/null 2>&1; then
    SSHPASSCMD=sshpass
else
(
  cd $USERDIR
  apt download sshpass
  dpkg -x sshpass*.deb $USERDIR
)
SSHPASSCMD=$USERDIR/usr/bin/sshpass
fi

if command -v pwgen > /dev/null 2>&1; then
    PWGEN=pwgen
else
(
  cd $USERDIR
  apt download pwgen
  dpkg -x pwgen*.deb $USERDIR
)
PWGEN=$USERDIR/usr/bin/pwgen
fi

if command -v docker-compose > /dev/null 2>&1; then
    echo "docker-compose est bien installé"
else
(
  cd $USERDIR
  mkdir -p $USERDIR/usr/bin
  curl -L "https://github.com/docker/compose/releases/download/v2.29.6/docker-compose-linux-x86_64" -o $USERDIR/usr/bin/docker-compose
  chmod +x $USERDIR/usr/bin/docker-compose
)
fi

if command -v docker > /dev/null 2>&1; then
    echo "docker est bien installé"
else
(
  cd $USERDIR
  apt download docker.io
  dpkg -x docker.io*.deb $USERDIR
)
fi
rm $USERDIR/*.deb 2> /dev/null

export PATH=$USERDIR/usr/bin:$PATH

# Nettoie les machines virtuelles qui auraient pu être déjà démarrées
echo "== Supprime toutes les machines virtuelles =="
vboxmanage list vms | gawk '{id=match($2,/\{(.*)\}/,m);print (m[1])}' | xargs -I {} vboxmanage unregistervm {} --delete &> /dev/null
sleep 10
# Crée la machine virtuelle
## Crée le disque support qui est vide pour l'instant
echo "== Crée la machine virtuelle =="
vboxmanage createmedium --filename $MACHINEDIR/disk.vmdk --format=VMDK --sizebyte 20971520000
## Crée la machine virtuelle
vboxmanage createvm --basefolder $MACHINEDIR --name $MACHINENAME --register
## Configure la machine virtuelle
vboxmanage modifyvm $MACHINENAME --firmware bios --bioslogofadein off --bioslogofadeout off --bioslogodisplaytime 0 --biosbootmenu disabled \
          --ostype Oracle_64 --cpus 2 --memory 2048 --acpi on --ioapic on --rtcuseutc on --natdnshostresolver1 off --natdnsproxy1 on \
          --cpuhotplug off --pae on --hpet on --hwvirtex on --nestedpaging on --largepages on --vtxvpid on --boot1 dvd
## Attache une interface réseau en nat à la machine virtuelle
vboxmanage modifyvm $MACHINENAME --nic1 nat --nictype1 82540EM --cableconnected1 on
## Ajoute un contrôleur de disque
vboxmanage storagectl $MACHINENAME --name SATA --add sata --hostiocache on
## Et attache l'image ISO comme si c'était un dvd et le disque créé précédemment
vboxmanage storageattach $MACHINENAME --storagectl SATA --port 0 --device 0 --type dvddrive --medium $MACHINEDIR/boot2docker.iso
vboxmanage storageattach $MACHINENAME --storagectl SATA --port 1 --device 0 --type hdd --medium $MACHINEDIR/disk.vmdk
## Configure le nécessaire pour disposer du disque local sur la machine virtuelle via /hostmachine
vboxmanage guestproperty set $MACHINENAME /VirtualBox/GuestAdd/SharedFolders/MountPrefix /
vboxmanage guestproperty set $MACHINENAME /VirtualBox/GuestAdd/SharedFolders/MountDir /
vboxmanage sharedfolder add $MACHINENAME --name hostmachine --hostpath / --automount
vboxmanage setextradata $MACHINENAME VBoxInternal2/SharedFoldersEnableSymlinksCreate/hostmachine 1
## Ajoute les redirections de ports pour les TP 2376 -> pour docker machine, et 22222, 8080, 9000 et 9090 pour les différents TP du cycle.
vboxmanage modifyvm $MACHINENAME --natpf1 ssh,tcp,127.0.0.1,41713,,22
vboxmanage modifyvm $MACHINENAME --natpf1 dockermachine,tcp,127.0.0.1,2376,,2376
vboxmanage modifyvm $MACHINENAME --natpf1 tp1ssh,tcp,,22222,,22222
vboxmanage modifyvm $MACHINENAME --natpf1 tp1reverseshell,tcp,,4444,,4444
vboxmanage modifyvm $MACHINENAME --natpf1 tp1web1,tcp,,8080,,8080
vboxmanage modifyvm $MACHINENAME --natpf1 tp1web2,tcp,,8888,,8888
vboxmanage modifyvm $MACHINENAME --natpf1 tpweb1,tcp,,9000,,9000
vboxmanage modifyvm $MACHINENAME --natpf1 tpweb2,tcp,,9090,,9090
vboxmanage modifyvm $MACHINENAME --natpf1 ctmich1,tcp,,3000,,3000
vboxmanage modifyvm $MACHINENAME --natpf1 ctmich2,tcp,,3001,,3001
vboxmanage modifyvm $MACHINENAME --natpf1 ctmich3,tcp,,9092,,9092

## Démarre la machine
echo "== Démarre la machine virtuelle =="
vboxmanage startvm $MACHINENAME --type headless
## Met en place une authentification par clé ssh
rm -f $MACHINEDIR/id_rsa*
echo "==> Génère une paire de clé ssh pour se connecter à la machine"
ssh-keygen -t rsa -N "" -f $MACHINEDIR/id_rsa
## Attend le démarrage pour cette mise en place
echo "==> Attend que le service ssh de la machine soit opérationnel"
until $SSHPASSCMD -p $PWDMACHINE ssh -p 41713 -n -o LogLevel=quiet -o "ConnectTimeout=120" -o "ConnectionAttempts=10" -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -o PubkeyAuthentication=no docker@localhost  "sudo chown docker .ssh"
do
    sleep 5
done
echo "==> Configure la machine virtuelle pour accepter la connexion par paire de clé ssh"
$SSHPASSCMD -p $PWDMACHINE scp -P 41713 -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -o PubkeyAuthentication=no $MACHINEDIR/id_rsa.pub docker@localhost:.ssh/authorized_keys
$SSHPASSCMD -p $PWDMACHINE scp -P 41713 -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -o PubkeyAuthentication=no $MACHINEDIR/id_rsa.pub docker@localhost:.ssh/authorized_keys2
echo "==> Change le mot de passe du compte docker de la machine virtuelle pour que ce ne soit pas celui par défaut"
ssh -p 41713 -n -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" docker@localhost -i "$MACHINEDIR/id_rsa" \
    "echo 'docker:$($PWGEN 40 1)' | sudo chpasswd"
# Création des certificats
echo "=> Création des certificats"
## Certificat de l'autorité de certification CA 
echo "==> Création du certificat racine de l'autorité de certification"
openssl req -x509 -days 1000 -newkey rsa:2048 \
    -keyout $MACHINEDIR/ca-key.pem -nodes \
    -out $MACHINEDIR/ca.pem -noenc \
    -subj "/O=$USER" \
    -addext "keyUsage=critical, digitalSignature, keyEncipherment, keyAgreement, keyCertSign"

## Certificat du Serveur (signé par le CA)
echo "==> Création du certificat du serveur docker"
openssl req -new  -newkey rsa:2048 \
    -keyout $MACHINEDIR/server-key.pem -nodes \
    -out $MACHINEDIR/server.csr -noenc \
    -subj "/O=$USER.$MACHINENAME"
cat <<EOF > $MACHINEDIR/server.ext
keyUsage = critical, digitalSignature, keyEncipherment, keyAgreement
extendedKeyUsage = serverAuth
basicConstraints = CA:FALSE
subjectAltName = DNS:localhost, IP:127.0.0.1
EOF
openssl x509 -req -days 999 \
        -in $MACHINEDIR/server.csr \
        -CA $MACHINEDIR/ca.pem \
        -CAkey $MACHINEDIR/ca-key.pem \
        -sha256 -CAcreateserial \
        -out $MACHINEDIR/server.pem \
        -extfile $MACHINEDIR/server.ext

## Certificat du client (signé par le CA)
echo "==> Création du certificat du client docker"
openssl req -new  -newkey rsa:2048 \
    -keyout $MACHINEDIR/key.pem -nodes\
    -out $MACHINEDIR/cert.csr -noenc \
    -subj "/O=$user.<bootstrap>"
cat <<EOF > $MACHINEDIR/cert.ext
keyUsage = critical, digitalSignature
extendedKeyUsage = clientAuth
basicConstraints = CA:FALSE
EOF
openssl x509 -req -days 999 \
        -in $MACHINEDIR/cert.csr \
        -CA $MACHINEDIR/ca.pem \
        -CAkey $MACHINEDIR/ca-key.pem \
        -sha256 -CAcreateserial \
        -out $MACHINEDIR/cert.pem \
        -extfile $MACHINEDIR/cert.ext

# Configure le daemon docker
echo "==> Configure le serveur docker"
echo "===> Nomme le serveur $MACHINENAME"
ssh -p 41713 -n -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" docker@localhost -i "$MACHINEDIR/id_rsa" \
    "sudo /usr/bin/sethostname $MACHINENAME && echo $MACHINENAME | sudo tee /var/lib/boot2docker/etc/hostname"
echo "===> Arrête le serveur docker pour mettre en place la nouvelle configuration"
ssh -p 41713 -n -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" docker@localhost -i "$MACHINEDIR/id_rsa" \
    "sudo /etc/init.d/docker stop"
echo "===> Supprime l'interface docker0 qui ne sera pas utile"
ssh -p 41713 -n -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" docker@localhost -i "$MACHINEDIR/id_rsa" \
    "if [ ! -z "$(ip link show docker0)" ]; then sudo ip link delete docker0; fi"

# Recopie les certificats ca.pem, server.pem, server-key.pem
echo "==> Configure le serveur docker (sur la machine virtuelle) pour prendre en compte les certificats"
scp -P 41713 -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -i "$MACHINEDIR/id_rsa" \
    $MACHINEDIR/ca.pem docker@localhost:/var/lib/boot2docker/
scp -P 41713 -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -i "$MACHINEDIR/id_rsa" \
    $MACHINEDIR/server.pem docker@localhost:/var/lib/boot2docker/
scp -P 41713 -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -i "$MACHINEDIR/id_rsa" \
    $MACHINEDIR/server-key.pem docker@localhost:/var/lib/boot2docker/
ssh -p 41713 -o LogLevel=quiet -n -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" docker@localhost -i "$MACHINEDIR/id_rsa" \
    "sudo chown root.root /var/lib/boot2docker/*.pem"

# Fichier de config boot2docker
cat <<EOF > $MACHINEDIR/profile
EXTRA_ARGS='
--label provider=virtualbox

'
CACERT=/var/lib/boot2docker/ca.pem
DOCKER_HOST='-H tcp://0.0.0.0:2376'
DOCKER_STORAGE=overlay2
DOCKER_TLS=auto
SERVERKEY=/var/lib/boot2docker/server-key.pem
SERVERCERT=/var/lib/boot2docker/server.pem


EOF
scp -P 41713 -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" -i "$MACHINEDIR/id_rsa" \
    $MACHINEDIR/profile docker@localhost:/var/lib/boot2docker/
ssh -p 41713 -n -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" docker@localhost -i "$MACHINEDIR/id_rsa" \
    "sudo chown root.root /var/lib/boot2docker/profile"

# Démarre le service
echo "=> Télécharge la nouvelle version de docker et redémarre le service docker"
ssh -p 41713 -n -o LogLevel=quiet -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" docker@localhost -i "$MACHINEDIR/id_rsa" \
    "curl -L https://download.docker.com/linux/static/stable/x86_64/docker-27.2.1.tgz | tar xvz;sudo mv docker/* /usr/local/bin;sudo /etc/init.d/docker start 2> /dev/null"

echo "=> Configure les variables d'environnements nécessaires"
export DOCKER_MACHINE_IP=127.0.0.1
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://$DOCKER_MACHINE_IP:2376"
export DOCKER_CERT_PATH="$MACHINEDIR"
export DOCKER_MACHINE_NAME="$MACHINENAME"

