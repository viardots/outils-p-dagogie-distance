#!/bin/bash -x
if [  -d /scratch ]; then
    TMPDATA=/scratch
else
    TMPDATA=/tmp
fi
USERDIR=${TMPDATA}/$(id -un)
DOCKERMACHINEDIR=$USERDIR/.docker/machine
DOCKERMACHINESDIR=$DOCKERMACHINEDIR/machines
CACHEDOCKER=$DOCKERMACHINEDIR/cache
MACHINENAME=default
MACHINEDIR=$DOCKERMACHINESDIR/$MACHINENAME
export DOCKER_MACHINE_IP=127.0.0.1
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://$DOCKER_MACHINE_IP:2376"
export DOCKER_CERT_PATH="$MACHINEDIR"
export DOCKER_MACHINE_NAME="$MACHINENAME"
export PATH=$USERDIR/usr/bin:$PATH
