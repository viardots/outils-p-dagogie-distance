#!/bin/bash
USERNAME=$(cat .username)
PASSWORD=$(cat .password)
TITRE=
DESCRIPTION=
PASSVIDEO=
RESTRICTED=
MOTSCLES=
CURSUS=M
DISCIPLINE=6
TYPE=2
VERB=
while [ "$1" != "" ]; do
    case $1 in
        -p)
        PASSVIDEO="$2"
        RESTRICTED="on"
        shift
        shift
        ;;
        -d)
        DESCRIPTION="$2"
        shift
        shift
        ;;
        -t)
        TITRE="$2"
        shift
        shift
        ;;
        --mots-cles)
        MOTSCLES="$2"
        shift
        shift
        ;;
        --niveau)
        CURSUS="$2" # 0, L, M, D ou 1 (autre)
        shift
        shift
        ;;
        --discipline)
        DISCIPLINE="$2" # 5 -> Physique, 6 -> Math/Info, 7 -> Ingénierie 
        shift
        shift
        ;;
        --type)
        TYPE="$2" # 2 -> Support pédagogique, 3 -> Tutoriel, 4 -> Production étudiante
        shift
        shift
        ;;
        -v)
        VERB=1
        shift
        ;;
        *)
        VIDEO=$1
        shift
        ;;
    esac
done

if [[ TITRE = "" ]]
then 
  TITRE=$VIDEO
fi

COOKIEFILE=$(mktemp TMPcookie.XXXXX)
HEADERDESTFILE=$(mktemp TMPheader.XXXXX)
PAGE=$(mktemp TMPpage.XXXXX)
# Se connecte au CAS de l'université pour pouvoir accéder au service POD
CASURL="https://cas-simsu.grenet.fr/login?service="
DEST="https://videos.univ-grenoble-alpes.fr/sso-cas/login/?next=https://videos.univ-grenoble-alpes.fr/video_edit/"
ENCODED_DEST=`echo "$DEST" | perl -p -e 's/([^A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg' | sed 's/%2E/./g' | sed 's/%0A//g'`
URL="$CASURL$ENCODED_DEST"
curl  -s -k -c $COOKIEFILE "$URL" -o "$PAGE"
CASID=$(grep name=.lt "$PAGE" | awk 'match($0,/value="(.*)"/,a){print a[1]}')
EXECID=$(grep execution "$PAGE" | awk 'match($0,/value="(.*)"/,a){print a[1]}' | perl -p -e 's/([^A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg' | sed 's/%2E/./g' | sed 's/%0A//g')
curl  -s -k --data-raw "locale=fr_FR&username=$USERNAME&password=$PASSWORD&lt=$CASID&execution=$EXECID&_eventId=submit&submit=SE+CONNECTER" \
     -i -b $COOKIEFILE -c $COOKIEFILE \
     "$URL" -D $HEADERDESTFILE -o "$PAGE"
URLDEST=$(grep Location $HEADERDESTFILE | awk 'match($0,/Location: (.*)$/,a){print a[1]}' | tr -d '\n' | tr -d '\r')
if [[ $VERB = "1" ]]; then echo $URLDEST; fi
# Se connecte au service de dépôt de vidéo sur la page sso-cas avec redirection vers les dépôts
curl  -k -s -b $COOKIEFILE -c $COOKIEFILE "$URLDEST" -D $HEADERDESTFILE 
URLDEST=$(grep Location $HEADERDESTFILE | awk 'match($0,/Location: (.*)$/,a){print a[1]}' | tr -d '\n' | tr -d '\r')
# Se connecte à la page de dépôt de vidéo pour récupérer le champ csrf
if [[ $VERB = "1" ]]; then echo $URLDEST; fi
curl  -k -s -b $COOKIEFILE -c $COOKIEFILE "$URLDEST" -D $HEADERDESTFILE -o "$PAGE"
#xmllint --html auth4.html --xpath '//*[@id="video_form"]//input'  2> /dev/null | sed "s/>/>\n/g" | awk 'match($0,/name="([^"]*)/,a){printf "-F \"%s=",a[1];if (match($0,/value="([^"]*)/,b)>0){printf "%s",b[1]}print "\" \\"}'
CSRFTOKEN=$(xmllint --html "$PAGE" --xpath '//*[@id="video_form"]//input[@name="csrfmiddlewaretoken"]' 2> /dev/null | awk 'match($0,/value="([^"]*)/,a){printf "%s",a[1]}')
if [[ $VERB = "1" ]]; then echo $CSRFTOKEN; fi

# Dépose la vidéo
curl -s  -k -s -b $COOKIEFILE -c $COOKIEFILE \
"https://videos.univ-grenoble-alpes.fr/video_edit/" -D $HEADERDESTFILE \
-H 'Cache-Control: no-cache' \
  -H 'User-Agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Mobile Safari/537.36' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: https://videos.univ-grenoble-alpes.fr/video_edit/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  -H 'dnt: 1' \
  -H 'sec-gpc: 1' \
-F "csrfmiddlewaretoken=$CSRFTOKEN" \
-F "title_fr=" \
-F "description_fr=" \
-F "video=@$VIDEO" \
-F "title=$TITRE" \
-F "title_en=" \
-F "title_nl=" \
-F "type=$TYPE" \
-F "discipline=$DISCIPLINE" \
-F "additional_owners=" \
-F "description=$DESCRIPTION" \
-F "description_en=" \
-F "description_nl=" \
-F "date_evt=$(date '+%d/%m/%y')" \
-F "initial-date_evt=$(date '+%Y-%m-%d')" \
-F "cursus=$CURSUS" \
-F "main_lang=fr" \
-F "tags=$MOTSCLES" \
-F "licence=by-nc-sa" \
-F "allow_downloading=$RESTRICTED" \
-F "is_restricted=$RESTRICTED" \
-F "password=$PASSVIDEO" \
-F "thumbnail=" \
-F "disable_comment=on" \
-F "_continue=" -o "$PAGE"
if [[ $VERB = "1" ]]; then echo $HEADERDESTFILE; fi

URLDEST=$(grep Location $HEADERDESTFILE | awk 'match($0,/Location: \/video_edit(.*)$/,a){print a[1]}' | tr -d '\n' | tr -d '\r')
echo "https://videos.univ-grenoble-alpes.fr/video$URLDEST"