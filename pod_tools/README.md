# Téléversement des vidéos sur https://videos.univ-grenoble-alpes.fr

Le dépôt de vidéo est vite fastidieux en passant par l'interface Web.

Pour faciliter le dépôt le script upload.sh fait l'essentiel : 
- Connection à la plateforme de vidéo de l'UGA
- Choix par défaut : 
    - "Supports pédagogiques"
    - "En français"
    - "Dans la disciple Math/Info"
    - "Niveau Master"
    - "Licence : ny-nc-sa -> Attribution - Pas d'utilisation commerciale - Partage dans les mêmes Conditions 4.0 International (CC BY-NC-SA 4.0)"
    - Avec un accès restreint (uniquement après login) si un mot de passe est mis

## Pré-requis

Avoir mis un fichier .username avec son login, et un fichier .password avec son mot de passe.

```bash
echo "viardots" > .username
echo "MONMOTDEPASSE" > .password
```

## Usage 

```bash
./upload.sh fichierVideo [-p mot-de-passe-acces-video] [-d "description-de-la-video"] \
            [-t "titre-video"] [--mots-cles "mots clés"] [--niveau L|M|D] [--discipline 5|6|7]
            [--type 2|3|4] 
# Pour les discipline 5 -> Physique, 6 -> Math/Info, 7 -> Ingénierie 
# Pour les types 2 -> Support pédagogique, 3 -> Tutoriel, 4 -> Production étudiante
```

Exemple : 
```bash
# Pour mettre en ligne à accès restreint avec mot de passe 
./upload ~/Video/MonCours1.mp4 -t "Cours 1 de bidule" -p "Ensimag2021"
# Pour mettre en ligne en public 
./upload ~/Video/MonCoursPublic.mp4 -t "Cours Public"
```

